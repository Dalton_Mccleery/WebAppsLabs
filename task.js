/*
 * task.js
 *
 * Contains implementation for a "task" "class"
 */

var Task, proto;

// Helper method. You should not need to change it.
// Use it in makeTaskFromString
function processString(s) {
   'use strict';
   var tags, title;

   tags = [];
   title = s.replace(/\s*#([a-zA-Z]+)/g, function(m, tag) {
      tags.push(tag);

      return '';
   });

   return { title: title, tags: tags };
}

/*
 *       Constructors
 */

function makeNewTask() {
   // This should initialize a new task. You should use `Object.create` to create it
   // and assign it properties as described above. While "title" and "completedTime" can be created via simple assignment,
   // "id" and "tags" will require the use of `Object.defineProperty`.
   // You must also use `Object.preventExtensions` before returning the object.
   // This function is exported to the world as `Task.new`.
   var newTaskObject;

   newTaskObject = Object.create(null);

   Object.defineProperty(newTaskObject, 'id', {
      get: (function() {
         var count;

         count = 0;

         return function() {
            count += 1;

            return count;
         };
      }())
   });

   Object.defineProperty(newTaskObject, 'tags', {
      configurable: false,
      enumerable: false,
      value: [],
      writable: true
   });

   newTaskObject['title'] = '';
   newTaskObject['completedTime'] = null;

   return Object.preventExtensions(newTaskObject);


}

function makeTaskFromObject(o) {

}

function makeTaskFromString(str) {

}


/*
 *       Prototype / Instance methods
 */

proto = {

   // Expects one string argument `s`. It should then set the object's title to equal the string after you have trimmed whitespace from both ends. 
   // It should return the object (`this`). If you look at the [String object's documentation]
   // (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String) 
   // you will find a string method that can help you trim whitespace easily.
   setTitle: function setTitle(s) {

   },


   // Returns a boolean value that indicates whether the object/task is "completed" or not, as indicated by the setting of `completedTime`.
   isCompleted: function isCompleted() {

      if (this.completedTime === null) {
         return false;
      }

      return true;

   },

   // If the task is completed, it "uncompletes" it by setting the `completedTime` back to `null`. 
   // If the task is not completed, it "completes" it by setting the `completedTime` to equal the current time. 
   // Returns the object (`this`).
   toggleCompleted: function toggleCompleted() {

      if (this.completedTime !== null) {
         this.completedTime = null;

         return this;
      }

   },

   // One string argument, to be interpreted as a tag. Returns a boolean depending on whether the tag appears in the tags list.
   hasTag: function hasTag(tag) {

      var i;

      for (i = 0; i < this.tags.length(); i += 1) {
         if (this.tags[i] === tag) {
            return true;
         }
      }

      return false;

   },

   // One string argument, to be interpreted as a tag. Adds the tag to the `tags` list, if it is not already there.
   addTag: function addTag(tag) {

      var i;

      for (i = 0; i < this.tags.length(); i += 1) {
         if (this.tags[i] === tag) {
            //already there
            return;
         }
      }

      this.tags[i] = tag;

   },

   // One string argument, to be interpreted as a tag. Removes the tag from the `tags` list, if it is there. Returns `this`.
   removeTag: function removeTag(tag) {

      var i;

      for (i = 0; i < this.tags.length(); i += 1) {
         if (this.tags[i] === tag) {
            // TODO: Remove tag
            return this;
         }
      }

   },

   // One string argument, to be interpreted as a tag. Toggles the status of the tag: If it isn't there, adds it. If it is there, removes it. Returns `this`.
   toggleTag: function toggleTag(tag) {

      if (this.hasTag(tag)) {
         this.removeTag(tag);
      }

      this.addTag(tag);

   },

   // One array argument, whose entries are strings (tags). Adds those tags from the list that are not already present. Returns `this`.
   addTags: function addTags(tags) {

      var i, j;

      //get rid of all the duplicates
      for (i = 0; i < this.tags.length(); i += 1) {
         for (j = 0; j < tags.length(); j += 1) {
            if (this.tags[i] === tags[j]) {
            // TODO: Remove tag from tags list
            }
         }
      }

      //add tags that weren't already there
      for (i = 0; i < tags.length(); i += 1) {
         this.addTag(tags[i]);
      }

      return this;

   },

   // One array argument, whose entries are strings (tags). Removes those tags from the list that are present. Returns `this`.
   removeTags: function removeTags(tags) {

      var i, j;

      for (i = 0; i < this.tags.length(); i += 1) {
         for (j = 0; j < tags.length(); j += 1) {
            if (this.tags[i] === tags[j]) {
            // TODO: Remove tag
            }
         }
      }

      return this;

   },

   // One array argument, whose entries are strings (tags). Toggles the presence of the tags in the list. Returns `this`.
   toggleTags: function toggleTags(tags) {

      var i, j;

      for (i = 0; i < this.tags.length(); i += 1) {
         for (j = 0; j < tags.length(); j += 1) {
            if (this.tags[i] === tags[j]) {
               this.toggleTag(tags[i]);
            }
         }
      }

   },

   // Returns a new task, with the same title, completion status and tag list as the original (`this`).
   clone: function clone(task) {

   }

};


// DO NOT MODIFY ANYTHING BELOW THIS LINE
Task = {
   new: makeNewTask,
   fromObject: makeTaskFromObject,
   fromString: makeTaskFromString
};

Object.defineProperty(Task, 'prototype', {
   value: proto,
   writable: false
});

module.exports = Task;
