/*
 * task.spec.js
 *
 * Test file for your task class
 */
var expect, Task;

expect = require('./chai.js').expect;

Task = require('./task.js');

// ADD YOUR TESTS HERE
var newTask;

newTask = Task.new();

describe('makeNewTask', function() {
   it('is defined', function() {
   	expect(function() { Task.new; }).to.not.throw(Error);
   });
   it('has an ID property', function() {
   	expect(newTask.id).to.exist;
   	//I've already called it above on line 15
   	expect(newTask.id).to.equal(2);
   });
   it('has a title property', function() {
   	expect(newTask.title).to.exist;
   	expect(newTask.title).to.equal("");
   });
   it('has an completedTime property', function() {
   	expect(newTask.completedTime).to.equal(null);
   });
   it('has an array of tags property', function() {
   	expect(newTask.tags).to.exist;
   });
});

describe('makeTaskFromObject', function() {
   it('is defined', function() {
   	expect(function() { Task.fromObject; }).to.not.throw(Error);
   });
});

describe('makeTaskFromString', function() {
   it('is defined', function() {
   	expect(function() { Task.fromString; }).to.not.throw(Error);
   });
});

describe('Proto methods', function() {
   it('is defined', function() {
   	expect(function() { Task.prototype; }).to.not.throw(Error);
   });

   it('has a working setTitle method', function() {
   	expect(Task.prototype.setTitle).to.exist;
   	// Task.prototype.setTitle("Task 2")
   	// expect(newTask.title).to.equal("Task 2");
   });
   it('has a working isCompleted method', function() {
   	expect(Task.prototype.isCompleted).to.exist;
   	expect(newTask.isCompleted()).to.equal(false);
   });
   it('has a working toggleCompleted method', function() {
   	expect(Task.prototype.toggleCompleted).to.exist;
   	// expect().to;
   });
   it('has a working hasTag method', function() {
   	expect(Task.prototype.hasTag).to.exist;
   	// expect().to;
   });
   it('has a working addTag method', function() {
   	expect(Task.prototype.addTag).to.exist;
   	// expect().to;
   });
   it('has a working removeTag method', function() {
   	expect(Task.prototype.removeTag).to.exist;
   	// expect().to;
   });
   it('has a working toggleTag method', function() {
   	expect(Task.prototype.toggleTag).to.exist;
   	// expect().to;
   });
   it('has a working addTags method', function() {
   	expect(Task.prototype.addTags).to.exist;
   	// expect().to;
   });
   it('has a working removeTags method', function() {
   	expect(Task.prototype.removeTags).to.exist;
   	// expect().to;
   });
   it('has a working toggleTags method', function() {
   	expect(Task.prototype.toggleTags).to.exist;
   	// expect().to;
   });
   it('has a working clone method', function() {
   	expect(Task.prototype.clone).to.exist;
   	// expect().to;
   });
});

